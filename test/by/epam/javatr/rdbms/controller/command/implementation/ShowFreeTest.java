package by.epam.javatr.rdbms.controller.command.implementation;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import by.epam.javatr.rdbms.bean.FreeBed;
import by.epam.javatr.rdbms.service.MutualInfoService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import by.epam.javatr.rdbms.service.factory.ServiceFactory;

public class ShowFreeTest {
	/***
	 * test to check is there response on query about free bed.
	 * It's also sql connection check.
	 */
	@Test
	public void test() {
		ServiceFactory sf = ServiceFactory.getInstance();
		MutualInfoService mService = sf.getMutInfService();
		try {
			 List<FreeBed> fr=mService.showFreeBeds();
			 assertTrue(fr.size()>0);
		} catch (ServiceException e) {
			fail("Exception occured "+e.getStackTrace());
		}
	}

}
