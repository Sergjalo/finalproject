package by.epam.javatr.rdbms.controller;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class Validation {
	Validator v;
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Before
	public void createV() {
		v=new Validator();
	}	

	@Test
	public void validateBedNegatives() {
	    thrown.expect(IllegalArgumentException.class);
	    thrown.expectMessage("negatives are not allowed");
		v.validateBeds("-60,45,67");
		fail("Should throw Exception");
	}

	@Test
	public void validateBedParsingNumber() {
		List<Integer> res;
		
		res=v.validateBeds("60,45,67");
		assertTrue(res.size()==3);
		res=v.validateBeds("  60, 45,   67  ");
		assertTrue(res.size()==3);
		res=v.validateBeds("60, 45");
		assertTrue(res.size()==2);
		res=v.validateBeds("60");
		assertTrue(res.size()==1);
	}

	@Test
	public void validateBedNotNumber() {
	    thrown.expect(IllegalArgumentException.class);
	    thrown.expectMessage("there is not valid number");
		v.validateBeds("8,dirt,67");
		fail("Should throw Exception");
	}

	@Test
	public void validateBedEmpty() {
	    thrown.expect(IllegalArgumentException.class);
	    thrown.expectMessage("there is not valid number");
		v.validateBeds("");
		fail("Should throw Exception");
	}

	@Test
	public void validateBedNull() {
	    thrown.expect(IllegalArgumentException.class);
	    thrown.expectMessage("nothing has been entered");
		v.validateBeds(null);
		fail("Should throw Exception");
	}
	
	@Test
	public void validateTypeNull() {
	    thrown.expect(IllegalArgumentException.class);
	    thrown.expectMessage("nothing has been entered");
		v.validateType(null);
		fail("Should throw Exception");
	}

	@Test
	public void validateType01() {
		assertTrue(v.validateType("1")==1);		
		assertTrue(v.validateType("0")==0);
	}

	@Test
	public void validateTypeNot01() {
	    thrown.expect(IllegalArgumentException.class);
	    thrown.expectMessage("is not valid number for type");
		v.validateType("4");
		fail("Should throw Exception");
	}

	@Test
	public void validateDate() {
		
	}
	
	
}
