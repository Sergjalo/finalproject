-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: jdbchotel
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beds`
--

DROP TABLE IF EXISTS `beds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beds` (
  `idbeds` int(11) NOT NULL AUTO_INCREMENT,
  `num_bed` int(11) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`idbeds`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beds`
--

LOCK TABLES `beds` WRITE;
/*!40000 ALTER TABLE `beds` DISABLE KEYS */;
INSERT INTO `beds` VALUES (1,100,NULL),(2,101,NULL),(3,102,NULL),(4,103,NULL),(5,104,NULL),(6,105,NULL),(7,200,NULL),(8,201,NULL),(9,202,NULL),(10,203,NULL),(11,204,NULL),(12,205,NULL),(13,206,NULL),(14,207,NULL);
/*!40000 ALTER TABLE `beds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `raiting` int(11) DEFAULT NULL,
  PRIMARY KEY (`idClient`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Постояльцев Диван Диваныч',100),(2,'Забегальцев Беглец Выбегайлович',2);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prices` (
  `idprices` int(11) NOT NULL AUTO_INCREMENT,
  `price` double DEFAULT NULL,
  `dstart` date DEFAULT NULL,
  `dend` date DEFAULT NULL,
  `booking_price` double DEFAULT NULL,
  PRIMARY KEY (`idprices`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prices`
--

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` VALUES (1,50,'2005-01-01','2005-02-01',5),(2,55,'2005-02-02','2009-03-31',6),(3,68,'2009-04-01','2012-12-31',5),(4,90,'2013-01-01','2015-12-31',10),(5,150,'2016-01-01','2016-12-31',12),(6,180,'2017-01-01','2017-03-01',14),(7,190,'2017-03-02',NULL,14);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `idrequest` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) DEFAULT NULL,
  `dstart` datetime DEFAULT NULL,
  `dend` datetime DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1',
  `type_req` tinyint(4) NOT NULL,
  `id_booking_req` int(11) DEFAULT NULL,
  `id_price` int(11) DEFAULT NULL,
  `date_request` datetime DEFAULT NULL,
  PRIMARY KEY (`idrequest`),
  KEY `fk_request_client_idx` (`id_client`),
  KEY `fk_request_statuses1_idx` (`id_status`),
  KEY `fk_request_price_idx` (`id_price`),
  KEY `fk_request_booking_idx` (`id_booking_req`),
  CONSTRAINT `fk_request_booking` FOREIGN KEY (`id_booking_req`) REFERENCES `request` (`idrequest`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_request_client` FOREIGN KEY (`id_client`) REFERENCES `clients` (`idClient`) ON DELETE CASCADE,
  CONSTRAINT `fk_request_price` FOREIGN KEY (`id_price`) REFERENCES `prices` (`idprices`) ON UPDATE CASCADE,
  CONSTRAINT `fk_request_statuses1` FOREIGN KEY (`id_status`) REFERENCES `statuses` (`idstatuses`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request`
--

LOCK TABLES `request` WRITE;
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
INSERT INTO `request` VALUES (3,1,'2015-03-05 00:00:00','2015-04-09 00:00:00',8,1,NULL,4,'2015-03-05 00:01:00'),(4,2,'2009-01-05 00:00:00','2009-10-05 00:00:00',8,0,NULL,2,'2009-01-01 08:07:00'),(5,2,'2009-01-05 00:00:00','2009-10-07 00:00:00',8,1,4,2,'2009-01-05 09:00:00'),(6,1,'2017-01-03 00:00:00','2017-03-05 00:00:00',5,0,NULL,6,'2017-02-03 10:02:48'),(7,1,'2017-03-02 00:00:00','2017-05-08 00:00:00',4,1,NULL,7,'2017-03-02 18:12:29'),(9,1,'2017-01-02 00:00:00','2017-01-03 00:00:00',8,1,NULL,6,'2017-03-28 19:10:35'),(10,1,'2017-01-02 00:00:00','2017-01-03 00:00:00',8,1,NULL,6,'2017-03-28 19:11:01'),(11,1,'2017-01-02 00:00:00','2017-01-03 00:00:00',8,1,NULL,6,'2017-03-28 19:12:21'),(12,1,'2017-01-02 00:00:00','2017-01-03 00:00:00',5,0,NULL,6,'2017-03-29 14:18:41'),(13,1,'2017-01-02 00:00:00','2017-01-03 00:00:00',1,1,12,6,'2017-03-29 14:20:18'),(48,1,'2016-12-30 00:00:00','2017-02-27 00:00:00',1,1,NULL,NULL,'2017-04-04 19:22:38'),(50,1,'2016-12-30 00:00:00','2017-02-27 00:00:00',1,1,NULL,NULL,'2017-04-11 19:48:38'),(85,1,NULL,NULL,1,1,NULL,NULL,'2017-05-03 18:10:39'),(93,1,'2010-09-30 00:00:00','2011-02-27 00:00:00',1,1,NULL,NULL,'2017-05-03 18:56:32');
/*!40000 ALTER TABLE `request` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `jdbchotel`.`request_BEFORE_INSERT` BEFORE INSERT ON `request` FOR EACH ROW
BEGIN
	SET NEW.date_request=now();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `requested_beds`
--

DROP TABLE IF EXISTS `requested_beds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requested_beds` (
  `id_request` int(11) NOT NULL,
  `id_bed` int(11) NOT NULL,
  KEY `fk_reqbeds_req_idx` (`id_request`),
  KEY `fk_reqbeds_bed_idx` (`id_bed`),
  CONSTRAINT `fk_reqbeds_bed` FOREIGN KEY (`id_bed`) REFERENCES `beds` (`idbeds`) ON UPDATE CASCADE,
  CONSTRAINT `fk_reqbeds_req` FOREIGN KEY (`id_request`) REFERENCES `request` (`idrequest`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requested_beds`
--

LOCK TABLES `requested_beds` WRITE;
/*!40000 ALTER TABLE `requested_beds` DISABLE KEYS */;
INSERT INTO `requested_beds` VALUES (3,2),(3,3),(3,6),(3,9),(5,4),(5,3),(6,3),(6,1),(7,4),(7,4),(11,1),(7,4),(12,3),(12,5),(48,1),(48,6),(48,7),(48,5),(50,1),(50,6),(50,7),(50,5),(85,1),(85,6),(85,7),(85,5),(93,1),(93,6),(93,7),(93,5);
/*!40000 ALTER TABLE `requested_beds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `idstatuses` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idstatuses`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (1,'Создан'),(2,'Подтвержден'),(3,'Оплачен'),(4,'Сдан'),(5,'Забронирован'),(6,'Принят'),(7,'Не принят'),(8,'Завершен');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `idusers` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Администратор'),(2,'Клиент');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-03 19:16:37
