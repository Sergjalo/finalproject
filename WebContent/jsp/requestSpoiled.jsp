<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="conflTag" 
           uri="/WEB-INF/tlds/ConflictInRequest" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/finaltask/mystyle3.css">
<title>Insert title here</title>
</head>
<body>
	<c:import url="/WEB-INF/js/script.html"/>	
	<c:import url="/header.html"/>	
	<div id="body">
		Conflicts detected!
		
		<conflTag:tblview lst="${requestconfl}" />

		<a href="index.jsp">home</a>
	</div>
    <c:import url="/footer.html"/>
</body>
</html>