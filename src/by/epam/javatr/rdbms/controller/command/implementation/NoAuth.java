package by.epam.javatr.rdbms.controller.command.implementation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import by.epam.javatr.rdbms.controller.command.Command;
/***
 * In case there was no authentification. 
 * Lets say it to user
 * @author Sergii_Kotov
 *
 */
public class NoAuth implements Command{
	private static final Logger logger = Logger.getLogger(NoAuth.class.getName());

	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) {
		try {
			req.getRequestDispatcher("/jsp/nologged.jsp").forward(req, resp);
		} catch (ServletException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
			try {
				req.getRequestDispatcher("/jsp/error.jsp").forward(req, resp);
			} catch(Exception ee) {
				logger.error(e);
			}
		}
	}

}
