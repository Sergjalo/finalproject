package by.epam.javatr.rdbms.controller.command.implementation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.epam.javatr.rdbms.controller.command.Command;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;

import org.apache.log4j.Logger;

public class DoLogin implements Command{
	private static final Logger logger = Logger.getLogger(DoLogin.class.getName());
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) {
		try {
			logger.debug("dispatch to login");
			boolean verified=true;
			// check log pass
			// some complex logic to verify login/pass
			// ...
			if (verified) {
				// write to the session about loggin
				HttpSession hs=req.getSession(true);
				// that attribute will be checked in the front controller
				hs.setAttribute("loggin",true);
				req.getRequestDispatcher("/jsp/logged.jsp").forward(req, resp);
			} else {
				req.getRequestDispatcher("/jsp/nologged.jsp").forward(req, resp);
			}
			
		} catch (ServletException | IOException e) {
			logger.error(e);
			try {
				req.getRequestDispatcher("/jsp/error.jsp").forward(req, resp);
			} catch(Exception ee) {
				logger.error(e);
			}
		}
	}

}
