package by.epam.javatr.rdbms.controller.command;
import java.util.HashMap;
import java.util.Map;

import by.epam.javatr.rdbms.controller.command.implementation.RequestRent;
import by.epam.javatr.rdbms.controller.command.implementation.ShowFree;
import by.epam.javatr.rdbms.controller.command.implementation.RequestRegPaymentBill;
import by.epam.javatr.rdbms.controller.command.implementation.DoLogin;
import by.epam.javatr.rdbms.controller.command.implementation.GetTodayPrice;
import by.epam.javatr.rdbms.controller.command.implementation.NoAuth;
import by.epam.javatr.rdbms.controller.command.implementation.RequestAccept;
import by.epam.javatr.rdbms.controller.command.implementation.RequestBanClient;
import by.epam.javatr.rdbms.controller.command.implementation.RequestBook;
import by.epam.javatr.rdbms.controller.command.implementation.RequestClose;
import by.epam.javatr.rdbms.controller.command.implementation.RequestCreate;
import by.epam.javatr.rdbms.controller.command.implementation.RequestCreatePaymentBill;
import by.epam.javatr.rdbms.controller.command.implementation.RequestDoNotAccept;
import by.epam.javatr.rdbms.controller.command.implementation.RequestGetSum;
import org.apache.log4j.Logger;

/***
 * Fabric for producing proper service method calls.
 * It uses MAP with keys as predefined Enum values and for values it uses 
 * instances of classes that implement Command interface.
 * @author Sergii_Kotov
 *
 */
public class CommandProvider {
	private static final Logger logger = Logger.getLogger(CommandProvider.class.getName());
	private final static CommandProvider instance = new CommandProvider();  
	enum Task{ WRONG_RESULT, GETTODAYPRICE,	REQUESTACCEPT,REQUESTBANCLIENT,	REQUESTBOOK,
		REQUESTCLOSE,	REQUESTCREATE,	REQUESTCREATEPAYMENTBILL,	REQUESTDONOTACCEPT,	REQUESTGETSUM,
		REQUESTREGPAYMENTBILL, REQUESTRENT, SHOWFREE, AUTHENTIFICATE, NOAUTH}
	private final Map <Task,Command> tasks = new HashMap<Task,Command>();
	
	private CommandProvider () {
		tasks.put(Task.NOAUTH,new NoAuth());
		tasks.put(Task.AUTHENTIFICATE,new DoLogin());
		tasks.put(Task.SHOWFREE,new ShowFree());
		tasks.put(Task.REQUESTRENT,new RequestRent());
		tasks.put(Task.REQUESTREGPAYMENTBILL,new RequestRegPaymentBill());
		tasks.put(Task.REQUESTGETSUM,new RequestGetSum());
		tasks.put(Task.REQUESTDONOTACCEPT,new RequestDoNotAccept());
		tasks.put(Task.REQUESTCREATEPAYMENTBILL,new RequestCreatePaymentBill());
		tasks.put(Task.REQUESTCREATE,new RequestCreate());
		tasks.put(Task.REQUESTCLOSE,new RequestClose());
		tasks.put(Task.REQUESTBOOK,new RequestBook());
		tasks.put(Task.REQUESTBANCLIENT,new RequestBanClient());
		tasks.put(Task.REQUESTACCEPT,new RequestAccept());
		tasks.put(Task.GETTODAYPRICE,new GetTodayPrice());
	}
	
	public static CommandProvider getInstance () {
		return instance;
	}
	
	public Command getCmd (String request){
		try {
			int pos=request.indexOf(" ");
			String command= pos>0?request.substring(0, pos):request;
			logger.debug("getCommand");
			return tasks.get(Task.valueOf(command));
		} catch (Exception e ){//(NullPointerException | IllegalArgumentException e) {
			logger.error(e);
			return null;
		}
	}

}
