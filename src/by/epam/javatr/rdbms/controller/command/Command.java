package by.epam.javatr.rdbms.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 * Pattern Command
 * @author Sergii_Kotov
 *
 */
public interface Command {
	/***
	 * Let make decision about what to do to command classes overriding this method 
	 * @param req
	 * @param resp
	 */
	public void execute(HttpServletRequest req, HttpServletResponse resp);
}
