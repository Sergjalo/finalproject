package by.epam.javatr.rdbms.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


public class Validator {
	private static final Logger logger = Logger.getLogger(Validator.class.getName());
	
	public List<Integer> validateBeds(String s) throws IllegalArgumentException{ 
		if (s==null) {
			throw new IllegalArgumentException("nothing has been entered!");
		}
		List<Integer> rez= new ArrayList<Integer>();
		
		String[] arBeds=s.trim().split("\\s*,\\s*");
		for (String sV:arBeds) {
			try {
				int k=Integer.parseInt(sV);
				if (k<0){
					logger.error("negatives are not allowed in a bed's list ("+sV+")");
					throw new IllegalArgumentException("negatives are not allowed in a bed's list ("+sV+")");
				} else {
					rez.add(k);
				}
			} catch (NumberFormatException e) {
				logger.error("there is not valid number (" +sV+ ") in a bed's list");
				throw new IllegalArgumentException("there is not valid number (" +sV+ ") in a bed's list");
			}
		}
	
	return  rez;
	}

	public Date validateDate(String s) throws IllegalArgumentException{ 
		if (s==null) {
			logger.error("nothing has been entered as a date");
			throw new IllegalArgumentException("nothing has been entered as a date");
		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dtS=null;
		try {
			dtS = new java.sql.Date(formatter.parse(s).getTime());
		} catch (ParseException e) {
			logger.error("there is not valid string (" +s+ ") as for date");
			throw new IllegalArgumentException("there is not valid string (" +s+ ") as for date");
		}
		return  dtS;
	}
	
	public int validateType(String typeB) throws IllegalArgumentException{
		if (typeB==null) {
			logger.error("nothing has been entered as a type");
			throw new IllegalArgumentException("nothing has been entered as a type");
		}
		try {
			int r=Integer.parseInt(typeB);
			if ((r!=0)&&(r!=1)) {
				logger.error("type this is not valid number for type (should be 1 or 0) ");
				throw new IllegalArgumentException("type this is not valid number for type (should be 1 or 0) ");
			}
			return r;
		} catch (NumberFormatException e) {
			logger.error("type should be number. This is not - " +typeB);
			throw new IllegalArgumentException("type should be number. This is not - " +typeB);
		}
	}
	
}
