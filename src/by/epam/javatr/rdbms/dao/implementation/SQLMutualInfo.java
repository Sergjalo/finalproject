package by.epam.javatr.rdbms.dao.implementation;

import by.epam.javatr.rdbms.bean.*;
import by.epam.javatr.rdbms.dao.factory.ConnectionPool;
import by.epam.javatr.rdbms.dao.*;
import by.epam.javatr.rdbms.dao.exception.ConnectionPoolException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/***
 * Specific sql request to MySQL database.
 * Here are examples of PreparedStatement usage
 * and calling stored procedures with CallableStatement
 * @author Sergii_Kotov
 *
 */
public class SQLMutualInfo implements MutualInfoDao {
	private static final Logger logger = Logger.getLogger(SQLMutualInfo.class.getName());
	ConnectionPool cp;

	public SQLMutualInfo (ConnectionPool c){
		this.cp=c;
	}	
	
	/***
	 * get statuses on every place 
	 */
	public List<FreeBed> showFreeBeds(){
		logger.debug("sql execution");
        List<FreeBed> l= new ArrayList <FreeBed>(); 
		Connection con;
		logger.debug("call stored proc get_free_beds"); 
    	String sql ="call get_free_beds();";
        try {
    		con = cp.takeConnection();
        	PreparedStatement statement = con.prepareStatement(sql); 
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
            	l.add(new FreeBed(rs.getInt("num_bed"),rs.getString("bedstatus"), 
            			rs.getString("orderid"),rs.getString("status"),rs.getString("period")));
            }
            cp.closeConnection(con);
		} catch (ConnectionPoolException e1) {
			logger.error(e1);
        } catch (Exception e) {        
			logger.error(e);
        }
        return l;
	}
	
	public double get_request_sum(int id){
		return 0;
	}
	
	/***
	 * текущая цена согласно прейскуранту. 
	 * PriceParametrs - это цена аренды, брони и код цены в справочнике 
	 */
	public BedPriceParametrs getTodayPrice() {
		Connection con;
		BedPriceParametrs p;
		logger.debug("call stored proc get_today_price"); 
    	String sql ="call get_today_price(?,?,?);"; //@p_rent, @p_book, @p_id
        try {
    		con = cp.takeConnection();
        	CallableStatement callSt = con.prepareCall(sql);
        	callSt.registerOutParameter("p_rent", java.sql.Types.DOUBLE);
        	callSt.registerOutParameter("p_book", java.sql.Types.DOUBLE);
        	callSt.registerOutParameter("p_id", java.sql.Types.INTEGER);
        	callSt.execute();
        	p=new BedPriceParametrs(callSt.getDouble(1),callSt.getDouble(2),callSt.getInt(3));
            cp.closeConnection(con);
        	return p;
		} catch (ConnectionPoolException e1) {
			logger.error(e1);
        } catch (Exception e) {        
			logger.error(e);
        }
		return null;
	}

}
