package by.epam.javatr.rdbms.dao.implementation;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.epam.javatr.rdbms.dao.factory.ConnectionPool;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;
import by.epam.javatr.rdbms.bean.*;
import by.epam.javatr.rdbms.dao.*;
import by.epam.javatr.rdbms.dao.exception.ConnectionPoolException;
import by.epam.javatr.rdbms.dao.exception.DaoException;
import org.apache.log4j.Logger;

/***
 * Specific sql request to MySQL database.
 * Here are examples of PreparedStatement usage
 * and calling stored procedures with CallableStatement 
 * @author Sergii_Kotov
 *
 */
public class SQLRequest implements RequestDao{
	private static final Logger logger = Logger.getLogger(SQLRequest.class.getName());
	ConnectionPool cp;

	public SQLRequest (ConnectionPool c){
		this.cp=c;
	}	

	@Override
	public Request get_request_sum(Request r) {
		Connection con;
		logger.debug("call stored proc get_request_sum"); 
    	String sql ="call get_request_sum(?,?,?,?,?);";
        try {
    		con = cp.takeConnection();
    		CallableStatement statement = con.prepareCall(sql);
    		statement.registerOutParameter("s_book", java.sql.Types.DOUBLE);
    		statement.registerOutParameter("s_rent_only", java.sql.Types.DOUBLE);
    		statement.registerOutParameter("s_rent", java.sql.Types.DOUBLE);
    		statement.registerOutParameter("s_topay", java.sql.Types.DOUBLE);
    		statement.setInt("rid", r.id);
    		statement.execute();
    		r.p_toPay=statement.getDouble("s_topay");
    		r.p_rent=statement.getDouble("s_rent");
    		r.p_rentOnly=statement.getDouble("s_rent_only");
    		r.p_book=statement.getDouble("s_book");
            cp.closeConnection(con);
		} catch (ConnectionPoolException e1) {
			logger.error(e1);
        } catch (Exception e) {        
			logger.error(e);
        }
        return r;
	}
	
	@Override
	public Request createRequest(Request r) throws DaoException{
		Connection con;
		int newId=0;
		boolean commitState;
        DaoException dExc = null;
        try {
        	// берем из пула
    		con = cp.takeConnection();
    		commitState=con.getAutoCommit();
    		con.setAutoCommit(false);
    		logger.debug("insert into request table");
    		// вставка в заказы
    		PreparedStatement st = con.prepareStatement("INSERT INTO request (id_client, type_req, dstart, dend) VALUES (?,?,?,?);");
            st.setInt(1,r.clientId);            
            st.setInt(2,r.type);
            st.setDate(3,r.dstart);
            st.setDate(4,r.dend);
            st.execute();
            // получим новый номер
            st = con.prepareStatement("select LAST_INSERT_ID()");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
            	newId= rs.getInt(1);
            }
            // вставим заказанные постельки
    		logger.debug("insert into requested_beds table");
            PreparedStatement stb=con.prepareStatement("INSERT INTO requested_beds (id_request, id_bed) "
            		+ "select ?, idbeds from beds where num_bed=?");
            for (int nm: r.beds) {
	            stb.setInt(1,newId);            
	            stb.setInt(2,nm);            
	          	stb.addBatch();    
            }
    		stb.executeBatch();
    		logger.debug("check request consistency");
    		// -------========     проверки
    		// если у проверяемого заказа есть предзаказ (бронь) непроведенная до сост. Бронирован, 
    		//то это непорядок. пусть сначала закроют бронь
    		int chk=0;
            st = con.prepareStatement("select count(1) "
            		+ "from request r inner join request r2 "
            		+ "on r.id_booking_req=r2.idrequest "
            		+ "where r.idrequest="+newId
            		+ " and r2.id_status<5;");
            rs = st.executeQuery();
            while (rs.next()) {
            	chk= rs.getInt(1);
            }
            if (chk>0) {
            	logger.error("please finish booking first");
            	throw new DaoException ("please finish booking first");
            }		
    		// если в заказе будет что-то что уже занято - вывести конфликтные места/сроки
			// как вывести целую таблицу? отдельный бин - ConflictsInRequest. А как же его вернуть то?
            st = con.prepareStatement(
            		"select rr.idrequest as reqCurrent,"
            + "            rr.num_bed,"
            + "            rw.idrequest as reqConflicted,"
            + "            rr.dstart, rr.dend,"
            + "            rw.dstart as dstartConflicted, rw.dend as dendConflicted,"
            + "            rw.type_req as conflType, rw.id_status as confStat, rw.date_request as confDateCreation"
            + "            from"
            + "            ("
            + "            select r.idrequest, rb.id_bed, r.dstart, r.dend, b.num_bed, r.type_req, r.id_status, r.date_request"
            + "            from request r inner join requested_beds rb"
            + "            on r.idrequest=rb.id_request"
            + "            inner join beds b"
            + "            on b.idbeds=rb.id_bed"
            + "           where r.idrequest<>?"
            + "            )rw inner join"
            + "            ("
            + "            select rb2.id_bed, r2.dstart, r2.dend, r2.idrequest, b.num_bed"
            + "            from request r2 inner join requested_beds rb2"
            + "            on r2.idrequest=rb2.id_request"
            + "            inner join beds b"
            + "            on b.idbeds=rb2.id_bed"
            + "            where r2.idrequest=?"
            + "            ) rr"
            + "            on rr.id_bed=rw.id_bed"
            + "            where (rr.dstart between rw.dstart and  rw.dend)"
            + "            or"
            + "            (rr.dend between rw.dstart and  rw.dend)"
            + "            or"
            + "            (rr.dstart <=rw.dstart and  rr.dend>=rw.dend);");
            st.setInt(1,newId);            
            st.setInt(2,newId);
            rs = st.executeQuery();
            chk=0;
            //а у нас исключение может отдавать специальный бин для описания датасета с другим списком - занятые кровати и мешаюшие заказы 
            ConflictsInRequest cnf=null;
            while (rs.next()) {
            	if (chk==0) {
            		logger.error("some beds are booked");
            		dExc = new DaoException ("some beds are booked. Chose another bed please. Detail info:\n");
            	}
           		cnf= new ConflictsInRequest( rs.getInt("reqCurrent"), rs.getInt("num_bed"),
            			rs.getInt("reqConflicted"),rs.getDate("dstart"),rs.getDate("dend"),rs.getDate("dstartConflicted"),rs.getDate("dendConflicted"),
            			rs.getInt("conflType"),rs.getInt("confStat"),rs.getDate("confDateCreation"));
           		dExc.addConflict(cnf);
            	chk=1;
            }
            if (chk>0) {
            	logger.debug("create list of conflict items");
            	con.rollback();
            	throw dExc;
            } else {
            	con.commit();
            }
    		con.setAutoCommit(commitState);
    		r.id=newId;
            cp.closeConnection(con);
		} catch (ConnectionPoolException e1) {
			logger.error(e1);
        } catch (Exception e) {        
			logger.error(e);
			throw dExc;
        }
        return r;
	}

	@Override
	public String createRequestPaymentBill(int reqId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String regRequestPaymentBill(int reqId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void rent(int reqId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void book(int reqId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void accept(int reqId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doNotAccept(int reqId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void banClient(int reqId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close(int reqId) {
		// TODO Auto-generated method stub
		
	}

}
