package by.epam.javatr.rdbms.dao.exception;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatr.rdbms.bean.*;

public class DaoException extends Exception{
	private List<ConflictsInRequest> conflicts=new ArrayList<ConflictsInRequest>();
	private static final long serialVersionUID = 1L;

	public DaoException() {
		super();
		
	}
	public DaoException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
	public DaoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
	public DaoException(String arg0) {
		super(arg0);
	}
	public DaoException(Throwable arg0) {
		super(arg0);
	}
	
	public void addConflict(ConflictsInRequest c){
		conflicts.add(c);
	}
	
	public List<ConflictsInRequest> getConflicts(){
		return conflicts;
	}
}