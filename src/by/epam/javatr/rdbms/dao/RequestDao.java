package by.epam.javatr.rdbms.dao;

import by.epam.javatr.rdbms.bean.Request;
import by.epam.javatr.rdbms.dao.exception.DaoException;
/***
 * interface for implementing actions with request lifecycle
 * @author Sergii_Kotov
 *
 */
public interface RequestDao {
	public Request get_request_sum(Request r) throws DaoException;
	public Request createRequest(Request r) throws DaoException;
	public String createRequestPaymentBill(int reqId) throws DaoException;
	public String regRequestPaymentBill(int reqId) throws DaoException;
	public void rent(int reqId) throws DaoException;
	public void book(int reqId) throws DaoException;
	public void accept(int reqId) throws DaoException;
	public void doNotAccept(int reqId) throws DaoException;
	public void banClient(int reqId) throws DaoException;
	public void close(int reqId) throws DaoException;
}
