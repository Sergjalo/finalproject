package by.epam.javatr.rdbms.dao.factory;

import java.sql.Connection;

import by.epam.javatr.rdbms.dao.exception.ConnectionPoolException;
import by.epam.javatr.rdbms.dao.factory.ConnectionPool;
import by.epam.javatr.rdbms.dao.implementation.SQLMutualInfo;
import by.epam.javatr.rdbms.dao.implementation.SQLRequest;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import by.epam.javatr.rdbms.controller.command.implementation.NoAuth;
import by.epam.javatr.rdbms.dao.*;
import org.apache.log4j.Logger;


public class DaoFactory {
	private static final Logger logger = Logger.getLogger(DaoFactory.class.getName());
    private static final DaoFactory instance = new DaoFactory();
    // можно было и не делать ConnectionPool синглтоном наверное. тут же только один раз создаем. 
    ConnectionPool cp = ConnectionPool.getInstance();
    // при выполнении каждого запроса беру коннекшн из пула и в нем выполняю.
    // после получения результата - возвращаю коннекшн в пул
    
    private MutualInfoDao sqlMutualInfo;
    private RequestDao sqlReq;
    
    private DaoFactory() {
		try {
			cp.initPoolData();
			sqlMutualInfo= new SQLMutualInfo(cp);
			sqlReq = new SQLRequest(cp);
			logger.debug("daofactory");
		} catch (ConnectionPoolException e) {
			logger.error(e);
		}
    }
    

    public static DaoFactory getInstance(){
    	return instance;
    }
    
    public MutualInfoDao getMutualInfo () {
    	logger.debug("getMut dao");
    	return sqlMutualInfo;
    }

    public RequestDao getRequest () {
    	return sqlReq;
    }
    
    public Connection getConnection () throws ConnectionPoolException {
    	return cp.takeConnection();
    }
    
}
