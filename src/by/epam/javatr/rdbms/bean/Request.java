package by.epam.javatr.rdbms.bean;



import java.io.Serializable;
import java.sql.Date;
import java.util.List;
/***
 * For requests on beds. 
 * 
 * @author Sergii_Kotov
 *
 */
public class Request  implements Serializable {
	private static final long serialVersionUID = 1L;
	public double p_rent; 
	public double p_book; 
	public double p_rentOnly; 
	public double p_toPay; 
	public int id; 
	public int clientId; 
	public Date dstart;
	public Date dend;
	public int statusId;
	public int type;
	public int idBooking;
	public List<Integer> beds;
	
	/***
	 * for request creation without list of requested beds
	 * @param id
	 * @param pr
	 * @param pb
	 * @param pro
	 * @param pPay
	 */
	public Request(int id, double pr, double pb, double pro, double pPay){
		p_rent=pr;
		p_book=pb;
		p_rentOnly=pro;
		p_toPay=pPay;
		this.id=id;
	}

	public Request(int id){
		p_rent=0;
		p_book=0;
		p_rentOnly=0;
		p_toPay=0;
		this.id=id;
	}

	/***
	 * For full request creation
	 * @param clId id client
	 * @param tp type of request (0 - for booking 1 - for renting)
	 * @param st start day
	 * @param en end day
	 * @param b list with beds nums
	 */
	public Request(int clId, int tp, Date st, Date en, List<Integer> b){
		clientId=clId;
		type=tp;
		beds=b;
		dstart=st;
		dend=en;
	}
	
	@Override
	public String toString() {
		return "Заказ " + id + ". К оплате " + p_toPay + " денег";
	}

	public double getP_rent() {
		return p_rent;
	}

	public void setP_rent(double p_rent) {
		this.p_rent = p_rent;
	}

	public double getP_book() {
		return p_book;
	}

	public void setP_book(double p_book) {
		this.p_book = p_book;
	}

	public double getP_rentOnly() {
		return p_rentOnly;
	}

	public void setP_rentOnly(double p_rentOnly) {
		this.p_rentOnly = p_rentOnly;
	}

	public double getP_toPay() {
		return p_toPay;
	}

	public void setP_toPay(double p_toPay) {
		this.p_toPay = p_toPay;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public Date getDstart() {
		return dstart;
	}

	public void setDstart(Date dstart) {
		this.dstart = dstart;
	}

	public Date getDend() {
		return dend;
	}

	public void setDend(Date dend) {
		this.dend = dend;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getIdBooking() {
		return idBooking;
	}

	public void setIdBooking(int idBooking) {
		this.idBooking = idBooking;
	}

	public List<Integer> getBeds() {
		return beds;
	}

	public void setBeds(List<Integer> beds) {
		this.beds = beds;
	}

}
