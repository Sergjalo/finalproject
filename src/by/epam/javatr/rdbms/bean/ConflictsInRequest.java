package by.epam.javatr.rdbms.bean;

import java.sql.Date;
import java.io.Serializable;
/***
 * Before start new request user should look at free bed's list and chose free bed. 
 * But in case of his/her fault or in case of collision of two requests 
 * there is check - is all beds from requests are free?
 * If not then each collision is represented as this bean.  
 * @author Sergii_Kotov
 *
 */

public class ConflictsInRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private int reqId;
	private int num_bed;
	private int confId;
	private Date dst;
	private Date den;
	private Date dstConf;
	private Date denConf;
	private int confType;
	private int confStat;
	private Date confDtCreate;
	
	public ConflictsInRequest(int reqId, int num_bed, int confId, Date dst, Date den, Date dstConf, Date denConf,
			int confType, int confStat, Date confDtCreate) {
		this.reqId = reqId;
		this.num_bed = num_bed;
		this.confId = confId;
		this.dst = dst;
		this.den = den;
		this.dstConf = dstConf;
		this.denConf = denConf;
		this.confType = confType;
		this.confStat = confStat;
		this.confDtCreate = confDtCreate;
	}

	@Override
	public String toString() {
      	  return String.format("%15s%15s%15s%20s%20s%20s%20s%15s%15s%20s\n",
     			reqId,num_bed,confId,dst,den,dstConf,denConf,confType,confStat,confDtCreate);
       	/* жалко что так неправильно делать. а то бы б удобно выводить бин в HTML таблицу... 
       	 * return String.format("<td>%s</td> <td>%s</td> <td>%s</td> "
       			+ "<td>%s</td> <td>%s</td> <td>%s</td> <td>%s</td> "
       			+ "<td>%s</td> <td>%s</td> <td>%s</td> \n",
    			reqId,num_bed,confId,dst,den,dstConf,denConf,confType,confStat,confDtCreate);
    			*/
	}

	public int getReqId() {
		return reqId;
	}
	public void setReqId(int reqId) {
		this.reqId = reqId;
	}
	public int getNum_bed() {
		return num_bed;
	}
	public void setNum_bed(int num_bed) {
		this.num_bed = num_bed;
	}
	public int getConfId() {
		return confId;
	}
	public void setConfId(int confId) {
		this.confId = confId;
	}
	public Date getDst() {
		return dst;
	}
	public void setDst(Date dst) {
		this.dst = dst;
	}
	public Date getDen() {
		return den;
	}
	public void setDen(Date den) {
		this.den = den;
	}
	public Date getDstConf() {
		return dstConf;
	}
	public void setDstConf(Date dstConf) {
		this.dstConf = dstConf;
	}
	public Date getDenConf() {
		return denConf;
	}
	public void setDenConf(Date denConf) {
		this.denConf = denConf;
	}
	public int getConfType() {
		return confType;
	}
	public void setConfType(int confType) {
		this.confType = confType;
	}
	public int getConfStat() {
		return confStat;
	}
	public void setConfStat(int confStat) {
		this.confStat = confStat;
	}
	public Date getConfDtCreate() {
		return confDtCreate;
	}
	public void setConfDtCreate(Date confDtCreate) {
		this.confDtCreate = confDtCreate;
	}
	
    	
}
