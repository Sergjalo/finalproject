package by.epam.javatr.rdbms.service;

import by.epam.javatr.rdbms.bean.Request;
import by.epam.javatr.rdbms.service.exception.ServiceException;

public interface RequestService {
	public Request get_request_sum(Request r) throws ServiceException;
	public Request createRequest(Request r) throws ServiceException;
	public String createRequestPaymentBill(int reqId) throws ServiceException;
	public String regRequestPaymentBill(int reqId) throws ServiceException;
	public void rent(int reqId) throws ServiceException;
	public void book(int reqId) throws ServiceException;
	public void accept(int reqId) throws ServiceException;
	public void doNotAccept(int reqId) throws ServiceException;
	public void banClient(int reqId) throws ServiceException;
	public void close(int reqId) throws ServiceException;
}
