package by.epam.javatr.rdbms.service.implementation;

import java.util.List;

import by.epam.javatr.rdbms.bean.BedPriceParametrs;
import by.epam.javatr.rdbms.bean.FreeBed;
import by.epam.javatr.rdbms.dao.MutualInfoDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;
import by.epam.javatr.rdbms.dao.implementation.SQLMutualInfo;
import by.epam.javatr.rdbms.service.MutualInfoService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import org.apache.log4j.Logger;

public class MutualInfoServiceImpl implements MutualInfoService {
	private static final Logger logger = Logger.getLogger(MutualInfoServiceImpl.class.getName());
	@Override
	public List<FreeBed> showFreeBeds() throws ServiceException {
		// тут будет проверка
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			MutualInfoDao mDao = daoObjectFactory.getMutualInfo();
			return mDao.showFreeBeds();
		} catch (DaoException e){
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public double get_request_sum(int id) throws ServiceException {
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			MutualInfoDao mDao = daoObjectFactory.getMutualInfo();
			return mDao.get_request_sum(id);
		} catch (DaoException e){
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	@Override
	public BedPriceParametrs getTodayPrice() throws ServiceException {
		return null;
	}
}
