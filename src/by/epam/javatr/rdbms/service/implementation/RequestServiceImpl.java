package by.epam.javatr.rdbms.service.implementation;

import by.epam.javatr.rdbms.bean.Request;
import by.epam.javatr.rdbms.dao.RequestDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;
import by.epam.javatr.rdbms.service.RequestService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import org.apache.log4j.Logger;

public class RequestServiceImpl implements RequestService {
	private static final Logger logger = Logger.getLogger(RequestServiceImpl.class.getName());
	public Request get_request_sum(Request r) throws ServiceException{
		return null;
	}

	@Override
	public Request createRequest(Request r) throws ServiceException {
		try {
			logger.debug("request create");
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			RequestDao mDao = daoObjectFactory.getRequest();
			return mDao.createRequest(r);
		} catch (DaoException e){
			logger.error("request creation failed "+e);
			throw new ServiceException(e);
		}
	}

	@Override
	public String createRequestPaymentBill(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String regRequestPaymentBill(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void rent(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void book(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void accept(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doNotAccept(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void banClient(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close(int reqId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}
}
