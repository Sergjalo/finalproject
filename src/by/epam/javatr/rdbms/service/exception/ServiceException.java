package by.epam.javatr.rdbms.service.exception;

import java.util.List;

import by.epam.javatr.rdbms.bean.ConflictsInRequest;
import by.epam.javatr.rdbms.bean.ConflictsList;
import by.epam.javatr.rdbms.dao.exception.DaoException;

public class ServiceException extends Exception{
	private static final long serialVersionUID = 1L;
	private ConflictsList conflicts;

	public ServiceException() {
		super();
	}
	public ServiceException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
	public ServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
	public ServiceException(String arg0) {
		super(arg0);
	}

	public ServiceException(Throwable arg0) {
		super(arg0);
		if( arg0 instanceof DaoException) {
			List<ConflictsInRequest> c=((DaoException)arg0).getConflicts();
			if (c!=null) {
				conflicts=new ConflictsList(c); 
			}
		}
	}
	
	public void addConflict(ConflictsInRequest c){
		conflicts.add(c);
	}
	
	public ConflictsList getConflicts(){
		return conflicts;
	}	
}
